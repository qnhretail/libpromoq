﻿namespace libPromoQ
{
    public enum TicketStatus
    {
        S10 = 10, // Wacht op registratie
        S15 = 15, // Wordt geregistreerd
        S20 = 20, // Wacht op identificatie klant
        S25 = 25, // Identificatie klant wordt gescand
        S30 = 30, // Wacht op verwerking
        S35 = 35, // Wordt verwerkt
        S40 = 40, // Wacht op afrekening
        S45 = 45, // Wordt afgerekend
        S46 = 46, // Wacht op koppeling klantenkaart
        S47 = 47, // Gekoppeld aan klantenkaart
        S50 = 50, // Wacht op afhandeling
        S55 = 55, // Wordt afgehandeld
        S60 = 60, // Afgehandeld
        S70 = 70  // Geannuleerd
    }
}