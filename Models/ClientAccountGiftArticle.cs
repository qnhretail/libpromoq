﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace libPromoQ
{
    public class ClientAccountGiftArticle
    {
        public ClientAccountGiftArticle(JToken jSonClientAccountGiftArticle)
        {
            giftArticleId = (long) jSonClientAccountGiftArticle["giftArticleId"];
            name = (string) jSonClientAccountGiftArticle["name"];
            productPlus = JsonConvert.DeserializeObject<string[]>((string) jSonClientAccountGiftArticle["productPlus"]);
            quantity = (long) jSonClientAccountGiftArticle["quantity"];
        }

        public long giftArticleId { get; set; }
        public string name { get; set; }
        public string[] productPlus { get; set; }
        public long quantity { get; set; }
    }
}