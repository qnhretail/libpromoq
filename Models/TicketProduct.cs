﻿using Newtonsoft.Json.Linq;

namespace libPromoQ
{
    public class TicketProduct
    {
        public TicketProduct(JToken jSonArticle)
        {
            id = (long) jSonArticle["id"];
            plu = (string) jSonArticle["plu"];
            description = (string) jSonArticle["description"];
            articleGroup = (long) jSonArticle["articleGroup"];
            price = (float) jSonArticle["price"];
            priceInclDiscount = (float) jSonArticle["priceInclDiscount"];
        }

        public long id { get; set; }
        public string plu { get; set; }
        public string description { get; set; }
        public long articleGroup { get; set; }
        public float price { get; set; }
        public float priceInclDiscount { get; set; }
    }
}