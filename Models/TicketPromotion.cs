﻿using System;
using System.Collections;
using Newtonsoft.Json.Linq;

namespace libPromoQ
{
    public class TicketPromotion
    {
        public TicketPromotion(JToken jSonTicketPromotion)
        {
            id = (long) jSonTicketPromotion["id"];
            promotionId = (long) jSonTicketPromotion["promotionId"];
            pointsRewarded = (long) jSonTicketPromotion["pointsRewarded"];
            totalPoints = (long) jSonTicketPromotion["totalPoints"];

            var jSonGiftArticles = jSonTicketPromotion["giftArticles"];
            giftArticles = new ArrayList();
            if (jSonGiftArticles != null)
            foreach (var jSonGiftArticle in jSonGiftArticles)
            {
                giftArticles.Add(new GiftArticle(jSonGiftArticle));
            }
            
            validUntil = DateTimeFactory.getDateTimeFromUnixDate((long) jSonTicketPromotion["validUntil"]);
            validateUntil = DateTimeFactory.getDateTimeFromUnixDate((long) jSonTicketPromotion["validateUntil"]);
        }

        public long id { get; set; }
        public long promotionId { get; set; }
        public long pointsRewarded { get; set; }
        public long totalPoints { get; set; }
        public ArrayList giftArticles { get; set; }
        public DateTime validUntil { get; set; }
        public DateTime validateUntil { get; set; }
    }
}