﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace libPromoQ
{
    public class GiftArticle
    {
        public GiftArticle(JToken jSonGiftArticle)
        {
            id = (long) jSonGiftArticle["id"];
            firmId = (long) jSonGiftArticle["firmId"];
            name = (string) jSonGiftArticle["name"];
            productPlus = JsonConvert.DeserializeObject<long[]>((string) jSonGiftArticle["productPlus"]);
        }

        public long id { get; set; }
        public long firmId { get; set; }
        public string name { get; set; }
        public long[] productPlus { get; set; }
    }
}