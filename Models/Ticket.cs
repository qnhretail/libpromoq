﻿using System;
using System.Collections;
using Newtonsoft.Json.Linq;

namespace libPromoQ
{
    public class Ticket
    {
        public long? id { get; set; }
        public TicketStatus? ticketStatus { get; set; }
        public string registerSerialNr { get; set; }
        public string clientCardNr { get; set; }
        public string staffMember { get; set; }
        public long? ticketNr { get; set; }
        public DateTime dateTime { get; set; }
        public ArrayList articles { get; set; }
        public ArrayList ticketPromotions { get; set; }
        public ArrayList clientAccountGiftArticles { get; set; }
        public ArrayList clientAcccountValuePromotions { get; set; }

        public Ticket(string json)
        {
            var jObject = JObject.Parse(json);
            var id = jObject["id"];
            if (id == null) this.id = null;
            else this.id = (long) jObject["id"];
            ticketStatus = null;
            registerSerialNr = (string) jObject["registerSerialNr"];
            clientCardNr = (string) jObject["clientCardNr"];
            staffMember = (string) jObject["staffMember"];
            ticketNr = (long) jObject["ticketNr"];
            var dateTime = (long?) jObject["dateTime"];
            this.dateTime = dateTime!= null ? DateTimeFactory.getDateTimeFromUnixDate((long) dateTime) : new DateTime();
            articles = new ArrayList();

            var jSonArticles = jObject["articles"];
            articles = new ArrayList();
            if (jSonArticles != null)
                foreach (var jSonArticle in jSonArticles)
                {
                    articles.Add(new TicketProduct(jSonArticle));
                }

            var jSonTicketPromotions = jObject["ticketPromotions"];
            ticketPromotions = new ArrayList();
            if (jSonTicketPromotions != null)
                foreach (var jSonTicketPromotion in jSonTicketPromotions)
                {
                    ticketPromotions.Add(new TicketPromotion(jSonTicketPromotion));
                }

            var jSonClientAccountGiftArticles = jObject["clientAccountGiftArticles"];
            clientAccountGiftArticles = new ArrayList();
            if (jSonClientAccountGiftArticles != null)
                foreach (var jSonClientAccountGiftArticle in jSonClientAccountGiftArticles)
                {
                    clientAccountGiftArticles.Add(new ClientAccountGiftArticle(jSonClientAccountGiftArticle));
                }

            var jSonClientAccountValuePromotions = jObject["clientAccountValuePromotions"];
            clientAcccountValuePromotions = new ArrayList();
            if (jSonClientAccountValuePromotions != null)
                foreach (var jSonClientAccountValuePromotion in jSonClientAccountValuePromotions)
                {
                    clientAcccountValuePromotions.Add(new ClientAccountValuePromotion(jSonClientAccountValuePromotion));
                }
        }
    }
}