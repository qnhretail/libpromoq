﻿using Newtonsoft.Json.Linq;

namespace libPromoQ
{
    public class ClientAccountValuePromotion
    {
        public ClientAccountValuePromotion(JToken jSonClientAccountValuePromotion)
        {
            promotionId = (long) jSonClientAccountValuePromotion["promotionId"];
            promotionName = (string) jSonClientAccountValuePromotion["promotionName"];
            points = (long) jSonClientAccountValuePromotion["points"];
        }

        public long promotionId { get; set; }
        public string promotionName { get; set; }
        public long points { get; set; }
    }
}