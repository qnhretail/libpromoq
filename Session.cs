﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace libPromoQ
{
    public class HttpSession
    {
        public HttpSession(string username, object password)
        {
            httpClient = new HttpClient(new HttpClientHandler());

            var byteArray = Encoding.ASCII.GetBytes(username + ":" + password);
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            
            ticketController = new TicketController(httpClient, baseUri);
            giftArticleController = new GiftArticleController(httpClient, baseUri);
        }

        private HttpClient httpClient;
        public TicketController ticketController;
        public GiftArticleController giftArticleController;
        public string baseUri = "http://localhost:8090/api/";
    }
}