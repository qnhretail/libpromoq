﻿using System;

namespace libPromoQ
{
    public static class DateTimeFactory
    {
        private static  DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public static DateTime getDateTimeFromUnixDate(long uDate)
        {
            return start.AddMilliseconds(uDate).ToLocalTime();
        }
    }
}