﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace libPromoQ
{
    public class TicketController: HttpController
    {
        public async Task<Ticket> RegisterTicket(Ticket ticket)
        {

            var jSonTicket = new StringContent(JsonConvert.SerializeObject(ticket), Encoding.UTF32, "application/json");
            var response = await httpClient.PostAsync(TARGETURL, jSonTicket);
            Console.WriteLine(response.StatusCode);
            return new Ticket(response.Content.ReadAsStringAsync().Result);
        }

        public async Task<Ticket> GetTicket(long id)
        {
            var response = await httpClient.GetAsync(TARGETURL + id);
            Console.WriteLine(response.StatusCode);
            return new Ticket(response.Content.ReadAsStringAsync().Result);
        }

        public async Task<Ticket> GetDummyTicket()
        {
            var response = await httpClient.GetAsync(TARGETURL + "0?dummy=true");
            var result = response.Content.ReadAsStringAsync().Result;
            Console.WriteLine(response.StatusCode);
            return new Ticket(result);
        }

        public async Task<Ticket> SettleTicket(long id)
        {
            var response = await httpClient.PostAsync(TARGETURL + "settle/" + id, null);
            Console.WriteLine(response.StatusCode);
            return new Ticket(response.Content.ReadAsStringAsync().Result);
        }

        public TicketController(HttpClient httpClient, string baseUri) : base(httpClient, baseUri)
        {
            TARGETURL += "ticket/";
        }
    }
}