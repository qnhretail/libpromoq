﻿using System.Net.Http;
using System.Threading.Tasks;

namespace libPromoQ
{
    public class GiftArticleController: HttpController
    {
        public async Task<Ticket> validateGiftArticle()
        {
            return null;
        }
        
        public GiftArticleController(HttpClient httpClient, string baseUri) : base(httpClient, baseUri)
        {
            TARGETURL += "promotionArticle/";
        }
    }
}