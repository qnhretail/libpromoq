﻿using System.Net.Http;

namespace libPromoQ
{
    public abstract class HttpController
    {
        protected HttpController(HttpClient httpClient, string baseUri)
        {
            this.httpClient = httpClient;
            TARGETURL = baseUri + "register/";
        }
        
        protected readonly HttpClient httpClient;
        protected string TARGETURL;
    }
}